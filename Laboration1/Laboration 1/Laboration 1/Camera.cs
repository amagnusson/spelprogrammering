﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Laboration_1
{
    class Camera
    {
        int sizeOfTile = 64;
        int borderSize = 64;
        float scale = 1;

        public Vector2 cordinateswhite(Vector2 piece)
        {
            float visualX = (borderSize + piece.X * sizeOfTile) * scale;
            float visualY = (borderSize + piece.Y * sizeOfTile) * scale;
            return new Vector2(visualX, visualY);
        }

        public Vector2 cordinatesblack(Vector2 piece)
        {
            float visualX = (borderSize + (7 - piece.X) * sizeOfTile) * scale;
            float visualY = (borderSize + (7 - piece.Y) * sizeOfTile) * scale;
            return new Vector2(visualX, visualY);
        }

        public void setresolution(int width, int height)
        {
            if (width < height)
                scale = (float)width / (borderSize * 2 + sizeOfTile * 8);
            else
                scale = (float)height / (borderSize * 2 + sizeOfTile * 8);
            Console.WriteLine(scale);
        }
    }
}
